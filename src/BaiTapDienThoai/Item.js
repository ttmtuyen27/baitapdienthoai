import React, { Component } from "react";
import ModalProduct from "./ModalProduct";

export default class Item extends Component {
  render() {
    let { hinhAnh, tenSP, maSP } = this.props.data;
    return (
      <div className="card col-4">
        <img src={hinhAnh} className="card-img-top" alt="..." />
        <div className="card-body">
          <h5 className="card-title">{tenSP}</h5>
          <a
            onClick={() => {
              this.props.handleOpenModal(this.props.data);
            }}
            className="btn btn-warning"
          >
            Xem chi tiết
          </a>
          <a
            onClick={() => {
              this.props.handleAddProductToCart(this.props.data);
            }}
            className="btn btn-success ml-4"
          >
            Add to cart
          </a>
        </div>
      </div>
    );
  }
}
