import { Popconfirm } from "antd";
import React, { Component } from "react";
import PopupConfirmDelete from "./PopupConfirmDelete";

export default class Cart extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <table className="table">
            <thead>
              <td>ID</td>
              <td>Tên sản phẩm</td>
              <td>Giá sản phẩm</td>
              <td>Số lượng</td>
              <td>Thao tác</td>
            </thead>
            <tbody>
              {this.props.gioHang.map((item) => {
                return (
                  <tr>
                    <td>{item.maSP}</td>
                    <td>{item.tenSP}</td>
                    <td>{item.giaBan}</td>
                    <td>
                      <button
                        onClick={() => {
                          this.props.handleTangGiamSoLuong(item.maSP, 1);
                        }}
                        className="btn btn-success"
                      >
                        Tăng
                      </button>
                      <span className="mx-4">{item.soLuong}</span>
                      <button
                        onClick={() => {
                          this.props.handleTangGiamSoLuong(item.maSP, -1);
                        }}
                        className="btn btn-secondary"
                      >
                        Giảm
                      </button>
                    </td>
                    <td>
                      <PopupConfirmDelete
                        handleDeleteProduct={this.props.handleDeleteProduct}
                        id={item.maSP}
                      >
                        <button className="btn btn-danger">Xóa</button>
                      </PopupConfirmDelete>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
