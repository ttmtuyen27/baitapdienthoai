import React, { Component } from "react";
import { Popconfirm, message } from "antd";

export default class PopupConfirmDelete extends Component {
  confirm = (e) => {
    message.success("Đã xóa sản phẩm ra khỏi giỏ hàng");
    this.props.handleDeleteProduct(this.props.id);
  };
  cancel = (e) => {
    message.error("Hủy");
  };
  render() {
    return (
      <Popconfirm
        title="Bạn có chắc chắn muốn xóa sản phẩm này ra khỏi giỏ hàng?"
        onConfirm={this.confirm}
        onCancel={this.cancel}
        okText="Đồng ý"
        cancelText="Hủy"
      >
        <a>{this.props.children}</a>
      </Popconfirm>
    );
  }
}
