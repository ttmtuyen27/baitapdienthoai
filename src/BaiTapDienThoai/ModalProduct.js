import React, { Component } from "react";
import { Modal, Button } from "antd";
export default class ModalProduct extends Component {
  render() {
    let data = this.props.sp;
    return (
      <Modal
        title={data.tenSP}
        visible={this.props.isOpenModal}
        onOk={this.props.handleCloseModal}
        onCancel={this.props.handleCloseModal}
      >
        <div className="row">
          <img className="col-5 h-100" src={data.hinhAnh} alt="" />
          <div className="right col-7">
            <h5>Thông số kỹ thuật</h5>
            <table className="table">
              <tbody>
                <tr>
                  <td>Màn hình</td>
                  <td>{data.manHinh}</td>
                </tr>
                <tr>
                  <td>Hệ điều hành</td>
                  <td>{data.heDieuHanh}</td>
                </tr>
                <tr>
                  <td>Camera trước</td>
                  <td>{data.cameraTruoc}</td>
                </tr>
                <tr>
                  <td>Camera trước</td>
                  <td>{data.cameraSau}</td>
                </tr>
                <tr>
                  <td>RAM</td>
                  <td>{data.ram}</td>
                </tr>
                <tr>
                  <td>ROM</td>
                  <td>{data.rom}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </Modal>
    );
  }
}
