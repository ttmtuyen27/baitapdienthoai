import React, { Component } from "react";
import Cart from "./Cart";
import { dataDienThoai } from "./dataDienThoai";
import ModalProduct from "./ModalProduct";
import ProductList from "./ProductList";

export default class BaiTapDienThoai extends Component {
  state = {
    dataDienThoai: dataDienThoai,
    isOpenModal: false,
    gioHang: [],
  };

  handleCloseModal = () => {
    this.setState({ isOpenModal: false });
  };

  sp = dataDienThoai[0];

  handleOpenModal = (sanPham) => {
    this.setState({ isOpenModal: true });
    this.sp = sanPham;
  };

  handleAddProductToCart = (sp) => {
    let cloneGioHang = [...this.state.gioHang];
    let index = cloneGioHang.findIndex((item) => {
      return item.maSP == sp.maSP;
    });
    if (index == -1) {
      let newSP = { ...sp, soLuong: 1 };
      cloneGioHang.push(newSP);
    } else cloneGioHang[index].soLuong += 1;
    this.setState({ gioHang: cloneGioHang });
  };

  handleDeleteProduct = (id) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.maSP == id;
    });
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang.splice(index, 1);
    this.setState({ gioHang: cloneGioHang });
  };

  handleTangGiamSoLuong = (id, giaTri) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.maSP == id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index != -1) {
      cloneGioHang[index].soLuong += giaTri;
    }
    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
    this.setState({ gioHang: cloneGioHang });
  };

  render() {
    return (
      <div className="container">
        <ModalProduct
          isOpenModal={this.state.isOpenModal}
          handleCloseModal={this.handleCloseModal}
          sp={this.sp}
        />
        <ProductList
          dataDienThoai={this.state.dataDienThoai}
          handleOpenModal={this.handleOpenModal}
          handleAddProductToCart={this.handleAddProductToCart}
        />
        <h3>Số lượng sản phẩm trong giỏ hàng: {this.state.gioHang.length}</h3>

        {this.state.gioHang.length > 0 && (
          <Cart
            gioHang={this.state.gioHang}
            handleDeleteProduct={this.handleDeleteProduct}
            handleTangGiamSoLuong={this.handleTangGiamSoLuong}
          />
        )}
      </div>
    );
  }
}
