import React, { Component } from "react";
import Item from "./Item";
import ModalProduct from "./ModalProduct";

export default class ProductList extends Component {
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          {this.props.dataDienThoai.map((item) => {
            return (
              <Item
                data={item}
                handleOpenModal={this.props.handleOpenModal}
                handleAddProductToCart={this.props.handleAddProductToCart}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
