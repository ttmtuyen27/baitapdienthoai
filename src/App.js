import logo from "./logo.svg";
import "./App.css";
import BaiTapDienThoai from "./BaiTapDienThoai/BaiTapDienThoai";
import "antd/dist/antd.css";

function App() {
  return (
    <div className="App">
      <BaiTapDienThoai />
    </div>
  );
}

export default App;
